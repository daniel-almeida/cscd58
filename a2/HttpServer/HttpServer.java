/**
 * HTTP Server, Single Threaded  
 * Usage:  java HTTPServer [port#  [http_root_path]]
 **/

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

public final class HttpServer {
	public static int serverPort = 35141; // default port
	public static String http_root_path = ""; // rooted default path in your mathlab area
	public static Date modifiedDate = null;
	
	public static void main(String args[]) throws Exception {

		ServerSocket welcomeSocket = null;
		Socket connectionSocket = null;

		// display error on server stdout if usage is incorrect
		if (args.length > 2) {
			//System.out.println("usage: java HTTPServer [port_# [http_root_path]]");
			System.exit(0);
		} else {
			if (args.length == 1)  {
				// allow the user to choose a different port
				if (args[0] != null) {
					serverPort = Integer.parseInt(args[0]);
				}
			} else {
				if (args.length == 2) {
					// allow the user to choose a different http_root_path and port
					if (args[0] != null && args[1] != null) {
						serverPort = Integer.parseInt(args[0]);
						http_root_path = args[1];
					}
				}
			}
			// adjust the path to avoid problems if the argument doesn't include a / at the end
			if (!http_root_path.equals("")) {
				int root_path_end = http_root_path.length() - 1;
				if(http_root_path.charAt(root_path_end) != '/'){
					http_root_path += "/";
				}
			}
		}

		// create server socket
		welcomeSocket = new ServerSocket(serverPort);

		// display server stdout message indicating listening
		// on port # with server root path ...
		//System.out.println("Server is listening on port " + serverPort);

		// server runs continuously
		while (true) {
			try {
				// take a waiting connection from the accepted queue
				connectionSocket = welcomeSocket.accept();
				// display on server stdout the request origin
				//System.out.println("Client IP: "
				//		+ connectionSocket.getInetAddress()
				//		+ ". Connection on port " + connectionSocket.getPort());

				// create buffered reader for client input
				BufferedReader inFromClient = new BufferedReader(
						new InputStreamReader(connectionSocket.getInputStream()));

				String requestLine = null; // the HTTP request line
				String requestHeader = null; // HTTP request header line

				/*
				 * Read the HTTP request line and display it on Server stdout.
				 * We will handle the request line below, but first, read and
				 * print to stdout any request headers (which we will ignore).
				 */
				requestLine = inFromClient.readLine();
				//System.out.println(requestLine);
				requestHeader = inFromClient.readLine();
				String lastModifiedDate = "";
				while (!requestHeader.equals("")) {
					//System.out.println(requestHeader);
					StringTokenizer tokenizedRequestHeader = new StringTokenizer(requestHeader);
					// evaluate the request header to find out if it's a conditional get
					if (tokenizedRequestHeader.nextToken().equals("If-Modified-Since:")) {
						SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
						while(tokenizedRequestHeader.hasMoreTokens()) {
							lastModifiedDate += " " + tokenizedRequestHeader.nextToken();
						}		
						modifiedDate = format.parse(lastModifiedDate.substring(1));		
					}
					requestHeader = inFromClient.readLine();
				}

				// tokenize the request
				StringTokenizer tokenizedLine = new StringTokenizer(requestLine);
				// process the request
				if (tokenizedLine.nextToken().equals("GET")) {
					String urlName = null;
					// parse URL to retrieve file name
					urlName = tokenizedLine.nextToken();

					if (urlName.startsWith("/") == true) {
						urlName = urlName.substring(1);
					}
					generateResponse(urlName, connectionSocket);
				} else {
					//System.out.println("Bad Request Message");
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		} // end while true

	} // end main

	private static void generateResponse(String urlName, Socket connectionSocket)
			throws Exception {
		// create an output stream
		DataOutputStream outToClient = new DataOutputStream(
				connectionSocket.getOutputStream());

		String fileName = "";
		int i = urlName.lastIndexOf('/');
		if (i > 0) {
		    fileName = urlName.substring(i+1);
		} else {
			fileName = urlName;
		}
		String fileLoc = http_root_path + fileName; //map urlName to rooted path		
		//System.out.println("Request Line: GET " + fileLoc);

		File file = new File(fileLoc);
		if (!file.isFile()) {
			// generate 404 File Not Found response header
			outToClient.writeBytes("HTTP/1.0 404 File Not Found\r\n");
			// and output a copy to server's stdout
			//System.out.println("HTTP/1.0 404 File Not Found\r\n");
		} else {
			// check if file was modified after the date from the conditional get
			if (modifiedDate != null && (file.lastModified() <= modifiedDate.getTime())) {
				// generate 304 Not Modified response header
				outToClient.writeBytes("HTTP/1.0 304 Not Modified\r\n");
				// output a copy to server's stdout
				//System.out.println("HTTP/1.0 304 Not Modified\r\n");
			} else {
				// get the requested file content
				int numOfBytes = (int) file.length();
				
				FileInputStream inFile = new FileInputStream(fileLoc);
				
				byte[] fileInBytes = new byte[numOfBytes];
				inFile.read(fileInBytes);
				
				// generate HTTP response line; output to stdout
				outToClient.writeBytes("HTTP/1.0 200 OK\r\n");
				//System.out.println("HTTP/1.0 200 OK\r\n");
				
				String extension = "";
				// get the file extension considering the substring after the last '.'
				int j = fileName.lastIndexOf('.');
				if (j > 0) {
				    extension = fileName.substring(j+1);
				}
				String contentType = "";
				if (extension.equals("html")) {
					contentType = "text/html";
				} else {
					if (extension.equals("txt")) {
						contentType = "text/plain";
					} else {
						if (extension.equals("css")) {
							contentType = "text/css";
						} else {
							if (extension.equals("js")) {
								contentType = "text/javascript";
							} else {
								if (extension.equals("jpg")) {
									contentType = "image/jpeg";
								}
							}
						}
					}
				}
				// generate HTTP Content-Type response header; output to stdout
				outToClient.writeBytes("Content-Type: " + contentType + "\r\n");
				//System.out.println("Content-Type: " + contentType + "\r\n");
				// generate HTTP Content-Length response header; output to stdout
				outToClient.writeBytes("Content-Length: " + numOfBytes + "\r\n");
				//System.out.println("Content-Length: " + numOfBytes + "\r\n");
				// send file content
				outToClient.writeBytes("\r\n");
				outToClient.write(fileInBytes, 0, numOfBytes);
				inFile.close(); // close file resource
			}
			
		}  // end else (file found case)
		modifiedDate = null;
		// close connectionSocket
		connectionSocket.close();
	} // end of generateResponse

} // end of class HTTPServer
