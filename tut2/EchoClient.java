import java.io.*;
import java.net.*;

/* This class is designed to work with EchoServer. It connects to a host, 
 * specified by the user, on port 9999. The program reads lines of input from
 * stdin and forwards them to the server. After each transmitted line, one
 * is read back from the sever and printed to stdout. */
public class EchoClient 
{
      public static void main(String[] args) throws IOException 
      {
	 Socket socket = null;
	 PrintWriter out = null;
	 BufferedReader in = null;

	 BufferedReader stdIn = new BufferedReader(
	    new InputStreamReader(System.in));
	 String hostname;

	 /* Grab the hostname */
	 if (args.length == 0)
	 {
	    System.out.println ("usage: java EchoClient hostname");
	    System.exit(1);
	 }
	 hostname = args[0];

	 try 
	 {
	    /* Open a socket to the specified destination */
	    socket = new Socket(hostname, 35141);
	    out = new PrintWriter(socket.getOutputStream(), true);
	    in = new BufferedReader(
	       new InputStreamReader(socket.getInputStream()));
	 } 
	 catch (UnknownHostException e) 
	 {
	    System.err.println("Don't know about host: " + hostname + ".");
	    System.exit(1);
	 } 
	 catch (IOException e) 
	 {
	    System.err.println("Couldn't get I/O for the connection to: " + 
			       hostname + ".");
	    System.exit(1);
	 }

	 /* Grab lines of input from the keyboard (ie stdin) and forward them
          * to the server. Then, one line of input is read back and printed
          * to stdout. This is repeated until stdin is closed (ie ^D). */
	 String line;
	 while ((line = stdIn.readLine()) != null)
	 {
	    out.println(line);
	    System.out.println("echo: " + in.readLine());
	 }

	 /* Close everything up. This actually terminates the TCP session  
	  * running between the this program and the server. */
	 out.close();
	 in.close();
	 stdIn.close();
	 socket.close();
      }
}

