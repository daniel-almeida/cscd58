import java.net.*;
import java.io.*;

/* This is a an example of a simple server. It opens a socket on port 9999, 
 * and waits for a client to connect. Once connected, the server reads input
 * from the client (one line at a time) and echos it back. */
public class EchoServer 
{
      public static void main(String[] args) throws IOException 
      {
	 int port = 35141;
	 ServerSocket serverSocket = null;
	 Socket clientSocket = null;
	 OutputStream cout = null;
	 InputStream cin = null;
	 int data;

	 /* Create a new server socket and connect it to the desired port */
	 try 
	 {
	    serverSocket = new ServerSocket(port);
	 } 
	 catch (IOException e) 
	 {
	    /* We have failed to create the socket. There are several reasons
	     * why this could be: 
	     *
	     *	-You do not have permission to create sockets
	     *  -You do not have permission to use the given port
	     *   (ie port < 1024 and you are not the root-user)
	     *	-The specified port is already in use (by another socket)
	     */
            System.err.println("Could not listen on port: " + port + ".");
            System.exit(1);
	 }

	 try {
	 while (true) 
	 {
	    /* Wait for a client to connect. The returned socket object can
	     * then be used to communicate with the client. */
	    try 
	    {
	       System.out.println("waiting for a connection");
	       clientSocket = serverSocket.accept();
	    } 
	    catch (IOException e) 
	    {
	       System.err.println("Accept failed: " + e);
	       System.exit(1);
	    }
	    System.out.println("client connected " + clientSocket);

	    /* These are the IO streams to use */
	    cout = clientSocket.getOutputStream();
	    cin = clientSocket.getInputStream();

	    /* Read in everything the client has to offer, and spit it right
	     * back, byte for byte. */
	    while ((data = cin.read()) >= 0) 
	    {
	       cout.write (data);
	    }

	    /* Close up everything */
	    cin.close();
	    cout.close();
	    clientSocket.close();
	 }
	 } finally {
	    serverSocket.close();
	 }
      }
}

