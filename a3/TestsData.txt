<----------------- HTTPS PIPE: ----------------->

<-------- RATE 10 -------->

Total: connections 30000 requests 52500 replies 30000 test-duration 749.963 s

Connection rate: 40.0 conn/s (25.0 ms/conn, <=4 concurrent connections)
Connection time [ms]: min 2.8 avg 16.9 max 3049.8 median 5.5 stddev 31.7
Connection time [ms]: connect 13.3
Connection length [replies/conn]: 1.000

Request rate: 70.0 req/s (14.3 ms/req)
Request size [B]: 126.0

Reply rate [replies/s]: min 39.9 avg 40.0 max 40.0 stddev 0.0 (149 samples)
Reply time [ms]: response 1.7 transfer 0.7
Reply size [B]: header 56.0 content 186.0 footer 0.0 (total 242.0)
Reply status: 1xx=0 2xx=22500 3xx=0 4xx=7500 5xx=0

CPU time [s]: user 180.19 system 498.07 (user 24.0% system 66.4% total 90.4%)
Net I/O: 18.1 KB/s (0.1*10^6 bps)

Errors: total 22500 client-timo 0 socket-timo 0 connrefused 0 connreset 22500
Errors: fd-unavail 0 addrunavail 0 ftab-full 0 other 0

Session rate [sess/s]: min 9.98 avg 10.00 max 10.00 stddev 0.00 (7500/7500)
Session: avg 4.00 connections/session
Session lifetime [s]: 0.1
Session failtime [s]: 0.0
Session length histogram: 0 0 0 0 7500

<-------- RATE 30 -------->

Total: connections 30000 requests 52500 replies 30000 test-duration 250.026 s

Connection rate: 120.0 conn/s (8.3 ms/conn, <=5 concurrent connections)
Connection time [ms]: min 2.6 avg 16.0 max 106.0 median 5.5 stddev 18.6
Connection time [ms]: connect 12.2
Connection length [replies/conn]: 1.000

Request rate: 210.0 req/s (4.8 ms/req)
Request size [B]: 126.0

Reply rate [replies/s]: min 119.2 avg 120.0 max 120.8 stddev 0.2 (50 samples)
Reply time [ms]: response 2.0 transfer 0.6
Reply size [B]: header 56.0 content 186.0 footer 0.0 (total 242.0)
Reply status: 1xx=0 2xx=22500 3xx=0 4xx=7500 5xx=0

CPU time [s]: user 69.32 system 165.80 (user 27.7% system 66.3% total 94.0%)
Net I/O: 54.4 KB/s (0.4*10^6 bps)

Errors: total 22500 client-timo 0 socket-timo 0 connrefused 0 connreset 22500
Errors: fd-unavail 0 addrunavail 0 ftab-full 0 other 0

Session rate [sess/s]: min 29.60 avg 30.00 max 30.40 stddev 0.11 (7500/7500)
Session: avg 4.00 connections/session
Session lifetime [s]: 0.1
Session failtime [s]: 0.0
Session length histogram: 0 0 0 0 7500

<-------- RATE 50 -------->

Total: connections 30000 requests 52500 replies 30000 test-duration 150.050 s

Connection rate: 199.9 conn/s (5.0 ms/conn, <=8 concurrent connections)
Connection time [ms]: min 2.7 avg 16.1 max 97.0 median 4.5 stddev 20.1
Connection time [ms]: connect 12.7
Connection length [replies/conn]: 1.000

Request rate: 349.9 req/s (2.9 ms/req)
Request size [B]: 126.0

Reply rate [replies/s]: min 198.4 avg 199.9 max 200.8 stddev 0.5 (30 samples)
Reply time [ms]: response 1.6 transfer 0.7
Reply size [B]: header 56.0 content 186.0 footer 0.0 (total 242.0)
Reply status: 1xx=0 2xx=22500 3xx=0 4xx=7500 5xx=0

CPU time [s]: user 43.36 system 100.50 (user 28.9% system 67.0% total 95.9%)
Net I/O: 90.6 KB/s (0.7*10^6 bps)

Errors: total 22500 client-timo 0 socket-timo 0 connrefused 0 connreset 22500
Errors: fd-unavail 0 addrunavail 0 ftab-full 0 other 0

Session rate [sess/s]: min 49.60 avg 49.98 max 50.40 stddev 0.15 (7500/7500)
Session: avg 4.00 connections/session
Session lifetime [s]: 0.1
Session failtime [s]: 0.0
Session length histogram: 0 0 0 0 7500

<-------- RATE 70 -------->

Total: connections 30000 requests 52500 replies 30000 test-duration 107.195 s

Connection rate: 279.9 conn/s (3.6 ms/conn, <=25 concurrent connections)
Connection time [ms]: min 2.6 avg 17.0 max 3012.8 median 6.5 stddev 26.1
Connection time [ms]: connect 12.7
Connection length [replies/conn]: 1.000

Request rate: 489.8 req/s (2.0 ms/req)
Request size [B]: 126.0

Reply rate [replies/s]: min 277.4 avg 279.9 max 280.6 stddev 0.7 (21 samples)
Reply time [ms]: response 2.1 transfer 0.8
Reply size [B]: header 56.0 content 186.0 footer 0.0 (total 242.0)
Reply status: 1xx=0 2xx=22500 3xx=0 4xx=7500 5xx=0

CPU time [s]: user 46.63 system 55.38 (user 43.5% system 51.7% total 95.2%)
Net I/O: 126.8 KB/s (1.0*10^6 bps)

Errors: total 22500 client-timo 0 socket-timo 0 connrefused 0 connreset 22500
Errors: fd-unavail 0 addrunavail 0 ftab-full 0 other 0

Session rate [sess/s]: min 69.20 avg 69.97 max 70.21 stddev 0.21 (7500/7500)
Session: avg 4.00 connections/session
Session lifetime [s]: 0.1
Session failtime [s]: 0.0
Session length histogram: 0 0 0 0 7500

<-------- RATE 100 -------->

Total: connections 30000 requests 52500 replies 30000 test-duration 75.087 s

Connection rate: 399.5 conn/s (2.5 ms/conn, <=24 concurrent connections)
Connection time [ms]: min 3.0 avg 17.5 max 115.3 median 6.5 stddev 20.1
Connection time [ms]: connect 13.1
Connection length [replies/conn]: 1.000

Request rate: 699.2 req/s (1.4 ms/req)
Request size [B]: 126.0

Reply rate [replies/s]: min 395.8 avg 399.7 max 400.2 stddev 1.1 (15 samples)
Reply time [ms]: response 2.0 transfer 1.2
Reply size [B]: header 56.0 content 186.0 footer 0.0 (total 242.0)
Reply status: 1xx=0 2xx=22500 3xx=0 4xx=7500 5xx=0

CPU time [s]: user 31.05 system 41.70 (user 41.4% system 55.5% total 96.9%)
Net I/O: 181.0 KB/s (1.5*10^6 bps)

Errors: total 22500 client-timo 0 socket-timo 0 connrefused 0 connreset 22500
Errors: fd-unavail 0 addrunavail 0 ftab-full 0 other 0

Session rate [sess/s]: min 98.80 avg 99.88 max 100.21 stddev 0.33 (7500/7500)
Session: avg 4.00 connections/session
Session lifetime [s]: 0.1
Session failtime [s]: 0.0
Session length histogram: 0 0 0 0 7500


<----------------- HTTP PIPE: ----------------->

<-------- RATE 10 -------->

Total: connections 30000 requests 52500 replies 30000 test-duration 749.910 s

Connection rate: 40.0 conn/s (25.0 ms/conn, <=3 concurrent connections)
Connection time [ms]: min 0.4 avg 3.9 max 3001.8 median 2.5 stddev 17.7
Connection time [ms]: connect 0.4
Connection length [replies/conn]: 1.000

Request rate: 70.0 req/s (14.3 ms/req)
Request size [B]: 126.0

Reply rate [replies/s]: min 39.2 avg 40.0 max 40.0 stddev 0.1 (149 samples)
Reply time [ms]: response 2.3 transfer 0.0
Reply size [B]: header 74.0 content 186.0 footer 0.0 (total 260.0)
Reply status: 1xx=0 2xx=22500 3xx=0 4xx=7500 5xx=0

CPU time [s]: user 186.04 system 535.27 (user 24.8% system 71.4% total 96.2%)
Net I/O: 18.8 KB/s (0.2*10^6 bps)

Errors: total 22500 client-timo 0 socket-timo 0 connrefused 0 connreset 22500
Errors: fd-unavail 0 addrunavail 0 ftab-full 0 other 0

Session rate [sess/s]: min 9.80 avg 10.00 max 10.00 stddev 0.02 (7500/7500)
Session: avg 4.00 connections/session
Session lifetime [s]: 0.0
Session failtime [s]: 0.0
Session length histogram: 0 0 0 0 7500

<-------- RATE 30 -------->

Total: connections 30000 requests 52500 replies 30000 test-duration 249.976 s

Connection rate: 120.0 conn/s (8.3 ms/conn, <=5 concurrent connections)
Connection time [ms]: min 0.8 avg 2.3 max 44.0 median 2.5 stddev 1.9
Connection time [ms]: connect 0.3
Connection length [replies/conn]: 1.000

Request rate: 210.0 req/s (4.8 ms/req)
Request size [B]: 126.0

Reply rate [replies/s]: min 119.6 avg 120.0 max 120.4 stddev 0.1 (49 samples)
Reply time [ms]: response 0.9 transfer 0.0
Reply size [B]: header 74.0 content 186.0 footer 0.0 (total 260.0)
Reply status: 1xx=0 2xx=22500 3xx=0 4xx=7500 5xx=0

CPU time [s]: user 61.84 system 183.46 (user 24.7% system 73.4% total 98.1%)
Net I/O: 56.5 KB/s (0.5*10^6 bps)

Errors: total 22500 client-timo 0 socket-timo 0 connrefused 0 connreset 22500
Errors: fd-unavail 0 addrunavail 0 ftab-full 0 other 0

Session rate [sess/s]: min 29.80 avg 30.00 max 30.20 stddev 0.04 (7500/7500)
Session: avg 4.00 connections/session
Session lifetime [s]: 0.0
Session failtime [s]: 0.0
Session length histogram: 0 0 0 0 7500

<-------- RATE 50 -------->

Total: connections 30000 requests 52500 replies 30000 test-duration 149.989 s

Connection rate: 200.0 conn/s (5.0 ms/conn, <=78 concurrent connections)
Connection time [ms]: min 0.5 avg 9.9 max 3010.8 median 2.5 stddev 93.4
Connection time [ms]: connect 2.7
Connection length [replies/conn]: 1.000

Request rate: 350.0 req/s (2.9 ms/req)
Request size [B]: 126.0

Reply rate [replies/s]: min 167.8 avg 200.0 max 232.2 stddev 8.6 (29 samples)
Reply time [ms]: response 6.1 transfer 0.0
Reply size [B]: header 74.0 content 186.0 footer 0.0 (total 260.0)
Reply status: 1xx=0 2xx=22500 3xx=0 4xx=7500 5xx=0

CPU time [s]: user 32.98 system 103.19 (user 22.0% system 68.8% total 90.8%)
Net I/O: 94.2 KB/s (0.8*10^6 bps)

Errors: total 22500 client-timo 0 socket-timo 0 connrefused 0 connreset 22500
Errors: fd-unavail 0 addrunavail 0 ftab-full 0 other 0

Session rate [sess/s]: min 38.00 avg 50.00 max 62.00 stddev 3.21 (7500/7500)
Session: avg 4.00 connections/session
Session lifetime [s]: 0.0
Session failtime [s]: 0.0
Session length histogram: 0 0 0 0 7500

<-------- RATE 70 -------->

Total: connections 30000 requests 52500 replies 30000 test-duration 107.137 s

Connection rate: 280.0 conn/s (3.6 ms/conn, <=54 concurrent connections)
Connection time [ms]: min 0.2 avg 17.1 max 313.1 median 2.5 stddev 33.4
Connection time [ms]: connect 0.3
Connection length [replies/conn]: 1.000

Request rate: 490.0 req/s (2.0 ms/req)
Request size [B]: 126.0

Reply rate [replies/s]: min 265.0 avg 280.0 max 295.0 stddev 4.9 (21 samples)
Reply time [ms]: response 15.7 transfer 0.0
Reply size [B]: header 74.0 content 186.0 footer 0.0 (total 260.0)
Reply status: 1xx=0 2xx=22500 3xx=0 4xx=7500 5xx=0

CPU time [s]: user 21.24 system 81.17 (user 19.8% system 75.8% total 95.6%)
Net I/O: 131.8 KB/s (1.1*10^6 bps)

Errors: total 22500 client-timo 0 socket-timo 0 connrefused 0 connreset 22500
Errors: fd-unavail 0 addrunavail 0 ftab-full 0 other 0

Session rate [sess/s]: min 64.20 avg 70.00 max 75.21 stddev 1.85 (7500/7500)
Session: avg 4.00 connections/session
Session lifetime [s]: 0.1
Session failtime [s]: 0.0
Session length histogram: 0 0 0 0 7500

<-------- RATE 100 -------->

Total: connections 30000 requests 52500 replies 30000 test-duration 74.999 s

Connection rate: 400.0 conn/s (2.5 ms/conn, <=4 concurrent connections)
Connection time [ms]: min 0.2 avg 2.0 max 15.0 median 2.5 stddev 1.0
Connection time [ms]: connect 0.3
Connection length [replies/conn]: 1.000

Request rate: 700.0 req/s (1.4 ms/req)
Request size [B]: 126.0

Reply rate [replies/s]: min 399.4 avg 400.0 max 400.6 stddev 0.3 (15 samples)
Reply time [ms]: response 0.7 transfer 0.0
Reply size [B]: header 74.0 content 186.0 footer 0.0 (total 260.0)
Reply status: 1xx=0 2xx=22500 3xx=0 4xx=7500 5xx=0

CPU time [s]: user 17.50 system 54.03 (user 23.3% system 72.0% total 95.4%)
Net I/O: 188.3 KB/s (1.5*10^6 bps)

Errors: total 22500 client-timo 0 socket-timo 0 connrefused 0 connreset 22500
Errors: fd-unavail 0 addrunavail 0 ftab-full 0 other 0

Session rate [sess/s]: min 99.81 avg 100.00 max 100.21 stddev 0.09 (7500/7500)
Session: avg 4.00 connections/session
Session lifetime [s]: 0.0
Session failtime [s]: 0.0
Session length histogram: 0 0 0 0 7500
