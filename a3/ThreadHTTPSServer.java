/**
 *   HTTP Server, Multi Threaded
 *   Usage:  java ThreadHTTPServer [port#  [http_root_path]]
 *
 **/

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

class HttpThread implements Runnable {

	Socket connSocket = null; 
	String http_Path = "";
	public static Date modifiedDate = null;
	Date startTime = null;
	
    // constructor to instantiate the HTTPThread object
    public HttpThread(Socket connectionSocket, String http_root_path) {
    	connSocket = connectionSocket;
    	http_Path = http_root_path;
    }

    public void run() {
		// invoke processRequest() to process the client request and then generateResponse()
		// to output the response message
    	try {
    		connSocket.setSoTimeout(120000);
			processRequest(this.connSocket);
		} catch (SocketTimeoutException toe){
			System.err.println("Thread interrupted. Send 408 to client...\n");
			DataOutputStream outToClient;
			try {
				outToClient = new DataOutputStream(
						connSocket.getOutputStream());
				// generate 408 Request Timeout response header
				outToClient.writeBytes("HTTP/1.0 408 Request Timeout\r\n");
				// and output a copy to server's stdout
				System.out.print("HTTP/1.0 408 Request Timeout\r\n");
    			outToClient.writeBytes("Connection: " + "Close" + "\r\n");
    			System.out.print("Connection: " + "Close" + "\r\n");
    			outToClient.writeBytes ("\r\n");
    			connSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    } 

    private void processRequest(Socket connectionSocket) throws Exception {
    	// same as in single-threaded (this code is inline in the starter code)
    	// create buffered reader for client input
		BufferedReader inFromClient = new BufferedReader(
				new InputStreamReader(connectionSocket.getInputStream()));
		
		Boolean keepalive = false;
		String requestLine = null; // the HTTP request line
		String requestHeader = null; // HTTP request header line
		
		while (!connectionSocket.isClosed()){
			/*
			 * Read the HTTP request line and display it on Server stdout.
			 * We will handle the request line below, but first, read and
			 * print to stdout any request headers (which we will ignore).
			 */
			requestLine = inFromClient.readLine();
			if (requestLine == null) {
				return;
			}
			requestHeader = inFromClient.readLine();
			String lastModifiedDate = "";
			String temp = "";
			while (!requestHeader.equals("")) {
				//System.out.println(requestHeader);
				StringTokenizer tokenizedRequestHeader = new StringTokenizer(requestHeader);
				// evaluate the request header to find out if it's a conditional get
				temp = tokenizedRequestHeader.nextToken();
				if (temp.toLowerCase().equals("if-modified-since:")) {
					SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
					while(tokenizedRequestHeader.hasMoreTokens()) {	
						lastModifiedDate += " " + tokenizedRequestHeader.nextToken();
					}	
					modifiedDate = format.parse(lastModifiedDate.substring(1));	
				} else {
					if (temp.toLowerCase().equals("connection:")) {
						temp = tokenizedRequestHeader.nextToken();
						if (temp.toLowerCase().equals("keep-alive")) {
							keepalive = true;
						} else {
							if (temp.toLowerCase().equals("close")) {
								keepalive = false;
							}
						}
					}
				}			
				requestHeader = inFromClient.readLine();
			}

			// tokenize the request
			StringTokenizer tokenizedLine = new StringTokenizer(requestLine);
			// process the request
			if (tokenizedLine.nextToken().equals("GET")) {
				String urlName = null;
				// parse URL to retrieve file name
				urlName = tokenizedLine.nextToken();

				if (urlName.startsWith("/") == true) {
					urlName = urlName.substring(1);
				}
				
				try {
					generateResponse(urlName, connectionSocket, keepalive);
					if (!keepalive) {
						connectionSocket.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}		
			} else {
				//System.out.println("Bad Request Message");
			}
		}
		
    }

    private void generateResponse(String urlName, Socket connectionSocket, boolean keepalive) throws Exception {
    	// create an output stream
		DataOutputStream outToClient = new DataOutputStream(
				connectionSocket.getOutputStream());
	
		String fileName = "";
		int i = urlName.lastIndexOf('/');
		if (i > 0) {
		    fileName = urlName.substring(i+1);
		} else {
			fileName = urlName;
		}
		String fileLoc = http_Path + fileName;		
		//System.out.println("\nRequest Line: GET " + fileLoc);

		File file = new File(fileLoc);
		if (!file.isFile()) {
			// generate 404 File Not Found response header
			outToClient.writeBytes("HTTP/1.0 404 File Not Found\r\n");
			// and output a copy to server's stdout
			//System.out.print("HTTP/1.0 404 File Not Found\r\n");
			if (keepalive) {
				outToClient.writeBytes("Connection: " + "Keep-Alive" + "\r\n");
				//System.out.print("Connection: " + "Keep-Alive" + "\r\n");
			} else {
				outToClient.writeBytes("Connection: " + "Close" + "\r\n");
				//System.out.print("Connection: " + "Close" + "\r\n");
			}
			outToClient.writeBytes ("\r\n");
		} else {	
			// check if file was modified after the date from the conditional get
			if (modifiedDate != null && (file.lastModified() <= modifiedDate.getTime())) {
				// generate 304 Not Modified response header
				outToClient.writeBytes("HTTP/1.0 304 Not Modified\r\n");
				// output a copy to server's stdout
				//System.out.print("HTTP/1.0 304 Not Modified\r\n");
				outToClient.writeBytes ("\r\n");
			} else {
				// get the requested file content
				int numOfBytes = (int) file.length();			
				FileInputStream inFile = new FileInputStream(fileLoc);			
				byte[] fileInBytes = new byte[numOfBytes];
				inFile.read(fileInBytes);
				
				// generate HTTP response line; output to stdout
				outToClient.writeBytes("HTTP/1.0 200 OK\r\n");
				//System.out.print("HTTP/1.0 200 OK\r\n");
				// generate HTTP Content-Type response header; output to
				// stdout
				String extension = "";
				
				int j = fileName.lastIndexOf('.');
				if (j > 0) {
				    extension = fileName.substring(j+1);
				}
				String contentType = "";
				if (extension.equals("html")) {
					contentType = "text/html";
				} else {
					if (extension.equals("txt")) {
						contentType = "text/plain";
					} else {
						if (extension.equals("css")) {
							contentType = "text/css";
						} else {
							if (extension.equals("js")) {
								contentType = "text/javascript";
							} else {
								if (extension.equals("jpg")) {
									contentType = "image/jpeg";
								}
							}
						}
					}
				}
				outToClient.writeBytes("Content-Type: " + contentType + "\r\n");
				//System.out.print("Content-Type: " + contentType + "\r\n");
				// generate HTTP Content-Length response header; output to
				// stdout
				outToClient.writeBytes("Content-Length: " + numOfBytes + "\r\n");
				//System.out.print("Content-Length: " + numOfBytes + "\r\n");
				// Add Connection:close or keep-alive
				if (keepalive) {
					outToClient.writeBytes("Connection: " + "Keep-Alive" + "\r\n");
					//System.out.print("Connection: " + "Keep-Alive" + "\r\n");
				} else {
					outToClient.writeBytes("Connection: " + "Close" + "\r\n");
					//System.out.print("Connection: " + "Close" + "\r\n");
				}
				outToClient.writeBytes ("\r\n");
				// send file content
				outToClient.write(fileInBytes, 0, numOfBytes);
				inFile.close(); // close file resource
			}
			
		}  // end else (file found case)
		modifiedDate = null;
	}
}

public final class ThreadHTTPSServer {
    
	public static int serverPort = 35141; // default port
	public static String http_root_path = "/cmshome/araujoa6/"; // rooted default path in your mathlab area
	public static String sslkeystore = "";
	public static String sslsocketkey = "";
	
	public static void main(String args[]) throws Exception  {

    	// process command-line options
    	// display error on server stdout if usage is incorrect
		
		if (args.length > 4 || args.length < 2) {
			System.out.println("usage: java HTTPServer sslkeystore sslsocketkey [port_# [http_root_path]]");
			System.exit(0);
		} else {
			if (args.length == 2) {
				if (args[0] != null && args[1] != null) {
					sslkeystore = args[0];
					sslsocketkey = args[1];
				} else {
					System.out.println("usage: java HTTPServer sslkeystore sslsocketkey [port_# [http_root_path]]");
					System.exit(0);
				}
			} else {
				if (args.length == 3)  {
					// allow the user to choose a different port
					if (args[2] != null) {
						serverPort = Integer.parseInt(args[2]);
					}
				} else {
					if (args.length == 4) {
						// allow the user to choose a different http_root_path and port
						if (args[2] != null && args[3] != null) {
							serverPort = Integer.parseInt(args[2]);
							http_root_path = args[3];
						}
					}
				}
			}
			
			// adjust the path to avoid problems if the argument doesn't include a / at the end
			if (!http_root_path.equals("")) {
				int root_path_end = http_root_path.length() - 1;
				if (http_root_path.charAt(root_path_end) != '/') {
					http_root_path += "/";
				}
			}
		}	
		
		System.setProperty("javax.net.ssl.keyStore", "httpserverkeys");
        System.setProperty("javax.net.ssl.keyStorePassword", "cscd58assignment3");
        SSLServerSocketFactory sslServerSocketFactory = (SSLServerSocketFactory)SSLServerSocketFactory.getDefault();
        SSLServerSocket sslWelcomeSocket = null;
        SSLSocket sslConnectionSocket = null;

		// create a listening SSLServerSocket
		//welcomeSocket = new ServerSocket(serverPort);
		sslWelcomeSocket = (SSLServerSocket)sslServerSocketFactory.createServerSocket(serverPort);
		
		// display server stdout message indicating listening
		// on port # with server root path ...
		System.out.println("Server is listening on port " + serverPort);
		while (true) {
		    // accept a connection
			sslConnectionSocket = (SSLSocket) sslWelcomeSocket.accept();
			System.out.println("Client IP: "
					+ sslConnectionSocket.getInetAddress()
					+ ". Connection on port " + sslConnectionSocket.getPort() + "\n");
			String[] arrayStrings = sslWelcomeSocket.getEnabledCipherSuites();
			for (int i = 0; i < arrayStrings.length; i++) {
				System.out.println("Enabled Cipher Suites: " + arrayStrings[i]);
			}
			arrayStrings = sslWelcomeSocket.getEnabledProtocols();
			for (int i = 0; i < arrayStrings.length; i++) {
				System.out.println("Enable Protocols: " + arrayStrings[i]);
			}
		    // Construct an HTTPThread object to process the accepted connection
			HttpThread httpthread = new HttpThread(sslConnectionSocket, http_root_path);
		    // Wrap the HTTPThread in a Thread object		
			Thread t = new Thread(httpthread, "HTTPS Request Thread");
		    // Start the thread.
			t.start();
		}
    } 
}
